class ProfilesController < ApplicationController
	before_action :set_profile, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!

	def index
	  if @profile.present?
	    redirect_to root_path
	  else
	    redirect_to new_profile_path
	  end
	end

	def new
		@profile = Profile.new()
		@user = @profile.build_user
	end

	def show
	  @profile = Profile.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @profile }
    end	
	end

	def create
		@profile = Profile.new(profile_params)

	end

	def update
	  respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to profile_edit_path, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
	end

	private

	def set_profile
    @profile = Profile.find(params[:id])
  end

  def profile_params
    params.require(:profile).permit(:full_name, :role, :biography, :coffee_price, :currency, :multiplier, :avatar)
  end
end
