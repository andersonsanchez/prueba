class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :validatable
  validates :email, uniqueness: { message: "Email ya Registrado" }
  # validate :check_password

  # def check_password
  #   @user = User.find_by(params[:id])
  #   errors.add(:encrypted_password, "Solo Tamaños de 20 o 40 pies") unless (@user.encrypted_password == params[:password])
  # end

end
