document.addEventListener("turbolinks:load", function() {
  console.log('It works!');

  (function(){
    const $formulario = document.getElementById("new_user");
    const $emailreg = document.getElementById("user_email");
    const $msgemail = document.getElementById("msgemail");

    $emailreg.addEventListener('change', function(event){
      const validaremail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
      if (!validaremail.test($emailreg.value)){
        $msgemail.innerHTML = "Formato de Correo Incorrecto";
        $msgemail.style.display;
        $emailreg.style.borderColor = 'red';
        event.preventDefault();
      }else{
        $msgemail.style.display = "none";
        $emailreg.style.borderColor = '';
      }
    })

    const validar = function(event){
      $emailreg(event);
    }

    $formulario.addEventListener('submit', validar);
  }());

})
