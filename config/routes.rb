Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index' #Esta es la que deberia ir, vista home
  resources :profiles
end
