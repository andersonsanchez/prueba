class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.string :full_name
      t.string :role
      t.string :gretting_message
      t.integer :kind
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
